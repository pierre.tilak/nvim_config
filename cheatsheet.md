## Change

ciw delete and insert current word
ci( delete and insert between parenthesis
ct[x] chance till the 'x' character

## Repeat

. Repeat the last command (like a change)

## Increment/Decrement

CTRL-A
CTRL-X

## Find

f[x] Find next occurence of 'x' character
; Go to next occurence in line
, Go to previous occurence in line

## Move vertically

zt : Move cursor to top
zb : Move cursor to bottom

### Jump commands

CTRL-O Go to [count] Older cursor position in jump list
(not a motion command).
CTRL-I Go to [count] newer cursor position in jump list
(not a motion command).

## Cool plugins

editor-config : Parse most common editor config and load them in vim

## Copy a file

on netrw, mark the file with "mt" (or "mf")
go to your new location and paste with "mc"

## Markers

ma : Set marker a
'a : Move to line of marker a
`a : Move to position of marker a
y`a: Yank the line of marker a

## TODO

Stop using the mouse for copying git files
Fix Sleep mode on my PC along with Power saving
Fix PyRight high cpu usage
Handling of different folders and git submodule while searching for file
(for inst, when I neeed to open code from another project, or search for source files in submodules)
